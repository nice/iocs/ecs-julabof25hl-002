#!/usr/bin/env iocsh.bash

# All require need to have version number
require(julabof25hl)
require(readhostip)
require(essioc)

# Set parameters when not using auto deployment
#epicsEnvSet(IPADDR,     "127.0.0.1")
#epicsEnvSet(IPPORT,     "9998")
epicsEnvSet(SYSTEM,     "ECS")
epicsEnvSet(DEVICE,     "JULABO-002")
epicsEnvSet(PREFIX,     "$(SYSTEM):$(DEVICE)")
epicsEnvSet(PORTNAME,   "$(PREFIX)")
epicsEnvSet(TEMPSCAN,   "1")
epicsEnvSet(CONFSCAN,   "10")
epicsEnvSet(LOCATION,   "$(SYSTEM); $(IPADDR)")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(julabof25hl_DIR)db")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

#Use for Lewis sim
epicsEnvSet(IPADDR, "127.0.0.1") #For use with Lewis simulator
epicsEnvSet(IPPORT, "9999")

#Specifying the TCP endpoint and port name
drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")

# Read host ip
iocshLoad("$(readhostip_DIR)/readhostip.iocsh", "P=$(SYSTEM):, R=$(DEVICE):")

#Load your database defining the EPICS records
iocshLoad("$(julabof25hl_DIR)julabof25hl.iocsh", "P=$(PREFIX), R=:, PORT=$(PORTNAME), ADDR=$(IPPORT), TEMPSCAN=$(TEMPSCAN), CONFSCAN=$(CONFSCAN)")
